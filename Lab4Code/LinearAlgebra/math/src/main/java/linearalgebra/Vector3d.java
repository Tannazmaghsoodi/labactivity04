//Tannaz Maghsoodi 2234772
package linearalgebra;
public class Vector3d {

    private double x;
    private double y;
    private double z;

    public Vector3d(double x, double y, double z){
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public double getX(){
        return this.x;
    }
    public double getY(){
        return this.y;
    }
    public double getZ(){
        return this.z;
    }

    public double magnitude() {
        return Math.sqrt((x * x) + (y * y) + (z * z));
    }

    public double dotProduct(Vector3d theOtherVector) {
        return x * theOtherVector.getX() + y * theOtherVector.getY() + z * theOtherVector.getZ();
    }

    public Vector3d add(Vector3d theOtherVector) {
        double theNewX = x + theOtherVector.getX();
        double theNewY = y + theOtherVector.getY();
        double theNewZ = z + theOtherVector.getZ();
        return new Vector3d(theNewX, theNewY, theNewZ);
    }
    

}
