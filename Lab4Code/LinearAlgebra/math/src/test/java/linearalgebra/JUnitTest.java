//Tannaz Maghsoodi 2234772
package linearalgebra;

import static org.junit.Assert.*;

import org.junit.Test;

public class JUnitTest {
    final double TOLERENCE = 0.0000001;

    //this test failed because we dont expect to get 6 but when the test fails it means this method is working properly 
    @Test
    public void testGetters() {
        Vector3d vector = new Vector3d(1, 2, 3);
        assertEquals(1, vector.getX(), TOLERENCE); //expect to return1
        assertEquals(2, vector.getY(), TOLERENCE); //expects to return 2
        assertEquals(3, vector.getZ(), TOLERENCE); //expects to return 3
    }

    //we expect to return 3.sth and this test must fail because I said im expecting 13
    @Test
    public void testMagnitudeMustReturn3() {
        Vector3d vector = new Vector3d(1, 2, 3);
        assertEquals(3.7416573867739413, vector.magnitude(), 0.001);
    }
    

    @Test
    public void testDotProduct() {
        Vector3d vector1 = new Vector3d(1, 2, 3);
        Vector3d vector2 = new Vector3d(4, 5, 6);
        assertEquals(32.0, vector1.dotProduct(vector2), 0.001);
    }

    @Test
    public void testAdd() {
        Vector3d vector1 = new Vector3d(1, 2, 3);
        Vector3d vector2 = new Vector3d(4, 5, 6);
        Vector3d sum = vector1.add(vector2);
        assertEquals(5.0, sum.getX(), 0.001);
        assertEquals(7.0, sum.getY(), 0.001);
        assertEquals(9.0, sum.getZ(), 0.001);
    }

}
